import csv
import time

import numpy as np

from src.util import EasyDict


def load_csv(filepath):
    """ Load a CSV into a list

    :param filepath:
    :return:
    """
    # We fetch all the data from the csv file
    with open(filepath, newline='') as csvfile:
        data = csv.reader(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        # We store them in a np array
        data_list = np.array([row for row in data])

    return data_list


def y_array_to_csv(y, filename):
    """ Write an output array into a csv file

    :param y:
    :param filename:
    :return:
    """
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quotechar='"')
        writer.writerow(('ID', 'consumption_1', 'consumption_2'))

        for idx, row in enumerate(y):
            w_row = [idx + 8760, int(round(row[0])), int(round(row[1]))]
            writer.writerow(w_row)


def str_list_to_type(list_str, new_type):
    """ Transform a list (array) of str to a list of the given types

    :param list_str:
    :param new_type:
    :return:
    """
    # In the case of a string, we have to append the value to the list, which is less efficient for performances
    if new_type != 'str':
        new_list = np.empty(np.shape(list_str), dtype=new_type)
    else:
        new_list = []

    missing_idx = []

    for idx, n in enumerate(list_str):
        try:
            next_val = n.astype(new_type)
        except ValueError:
            # We act depending on the new type
            missing_idx.append(idx)
            if new_type == 'int' or new_type == 'float':
                if idx >= 24:
                    next_val = new_list[idx - 24]
                else:
                    next_val = 0
            elif new_type == 'str':
                next_val = ''
            else:
                next_val = ''

        # If the new type is string, we append
        if new_type != 'str':
            new_list[idx] = next_val
        else:
            new_list.append(next_val)

    if new_type == 'str':
        new_list = np.array(new_list)

    # Dealing with missing values
    current_serie = []
    for i in range(len(missing_idx)):
        current_serie.append(missing_idx[i])
        if i == len(missing_idx) - 1:
            end_serie = True
        elif missing_idx[i + 1] == missing_idx[i] + 1:
            end_serie = False
        else:
            end_serie = True

        if end_serie:
            # Si il ne manque que peu de valeurs consécutives, on les extrapole linéairement
            if len(current_serie) <= 5:
                if current_serie[0] != 0 and current_serie[-1] != len(new_list):
                    a_start = new_list[current_serie[0] - 1]
                    a_end = new_list[current_serie[-1] + 1]

                    lin_interp = np.linspace(a_start, a_end, len(current_serie) + 2)
                    lin_interp = lin_interp[1:-1]

                    new_list[current_serie] = lin_interp
                else:
                    new_list[current_serie] = new_list[current_serie[0] - 1]
            # Si il manque plus de valeurs, on recopie celles de 24h plus tôt
            else:
                current_serie_24 = [n - 24 for n in current_serie]
                for j in range(int(len(current_serie) / 24)):
                    new_list[current_serie[24 * j: 24 * (j + 1)]] = new_list[current_serie_24[24 * j: 24 * (j + 1)]]
                if len(current_serie) % 24 != 0:
                    last_j = int(len(current_serie) / 24)
                    new_list[current_serie[24 * last_j:]] = new_list[current_serie_24[24 * last_j:]]

            current_serie = []

    return new_list


def load_x_csv(filepath):
    """ Load the csv file of inpuy into a dict

    :param filepath:
    :return:
    """
    x_list = load_csv(filepath)

    col_types = ['int',  # ID
                 'str',  # Timestamp (before other treatment)
                 'float', 'float', 'float', 'float', 'float',  # Temperatures
                 'str', 'str', 'str', 'str', 'str',  # Location, before other treatment
                 'int', 'int', 'int']  # Consumptions

    # Finally, we store them in an EasyDict to make them easier to use
    x_dict = EasyDict()

    for idx, col_name in enumerate(x_list[0]):
        list_str = x_list[1:, idx]
        x_dict[col_name] = str_list_to_type(list_str, col_types[idx])

    return x_dict


def load_y_csv(filepath):
    """ Load the csv file of output into a dict

    :param filepath:
    :return:
    """
    y_list = load_csv(filepath)

    col_types = ['int',  # ID
                 'int', 'int']  # Consumptions

    # Finally, we store them in an EasyDict to make them easier to use
    y_dict = EasyDict()

    for idx, col_name in enumerate(y_list[0]):
        list_str = y_list[1:, idx]
        y_dict[col_name] = str_list_to_type(list_str, col_types[idx])

    return y_dict


def dict_to_array(data_dict, col_to_keep):
    """ Create the dataset from the dictionary, by keeping only the given column names

    :param data_dict:
    :param col_to_keep:
    :return:
    """

    nb_col = len(col_to_keep)
    nb_row = data_dict[col_to_keep[0]].shape[0]

    data_array = np.empty(shape=(nb_row, nb_col))
    for idx, name in enumerate(col_to_keep):
        data_array[:, idx] = data_dict[name]

    return data_array


def separate_train_test(x, y, test_proportion=0.15, rdom_seed=123456):
    """ Randomly separate data into train and test

    :param y:
    :param rdom_seed:
    :param x:
    :param test_proportion:
    :return:
    """

    idx = np.linspace(0, x.shape[0])
    split_idx = int(x.shape[0] * test_proportion)

    np.random.seed(rdom_seed)
    np.random.shuffle(idx)
    idx_test = idx[:split_idx]
    idx_train = idx[split_idx:]

    x_train = x[idx_train, :]
    x_test = x[idx_test, :]

    y_train = y[idx_train, :]
    y_test = y[idx_test, :]

    return x_train, x_test, y_train, y_test


def read_timestamp(data_dict, col_name='timestamp'):
    """ Takes the data and add new column for hours, day, month and a bool for weekend.

    :param data_dict:
    :param col_name:
    :return:
    """
    time_array = data_dict[col_name]

    hour_array = np.zeros(time_array.shape, dtype=int)
    day_array = np.zeros(time_array.shape, dtype=int)
    month_array = np.zeros(time_array.shape, dtype=int)

    bool_weekend = np.zeros(time_array.shape, dtype=int)
    bool_week = np.zeros(time_array.shape, dtype=int)

    bool_daytime = np.zeros(time_array.shape, dtype=int)
    bool_night = np.zeros(time_array.shape, dtype=int)

    for idx, time_str in enumerate(time_array):
        time_time = time.strptime(time_str, "%Y-%m-%dT%H:%M:%S.0")

        hour_array[idx] = time_time.tm_hour
        day_array[idx] = time_time.tm_mday
        month_array[idx] = time_time.tm_mon

        is_weekend = (time_time.tm_wday >= 5)
        bool_weekend[idx] = is_weekend
        bool_week[idx] = not is_weekend

        is_daytime = (7 <= time_time.tm_hour <= 19)
        bool_daytime[idx] = is_daytime
        bool_night[idx] = not is_daytime

    data_dict.hour = hour_array
    data_dict.day = day_array
    data_dict.month = month_array

    data_dict.weekend = bool_weekend
    data_dict.week = bool_weekend

    data_dict.daytime = bool_daytime
    data_dict.night = bool_night


def define_holidays(data_dict, start_point_list, end_point_list):
    """ Defines the boolean 'holidays' based on the list given.
    For example, for two holidays, one from point 1000 to 1200, and the other from 3200 to 3900, the lists should be:
        start_point_list = [1000, 3200]
        end_point_list = [1200, 3900]

    :param data_dict:
    :param start_point_list:
    :param end_point_list:
    :return:
    """

    assert len(start_point_list) == len(end_point_list)

    holidays_array = np.zeros(data_dict.ID.shape)

    for idx in range(len(start_point_list)):
        start = start_point_list[idx]
        end = end_point_list[idx]

        holidays_array[start:end] = True

    data_dict.holidays = holidays_array


def multi_hours_data(x, delta_h):
    """ Modify the data such that each line corresponds to multiple hours

    :param delta_h:
    :param x:
    :return:
    """

    n_samp = x.shape[0]
    n_dim = x.shape[1]

    new_x = np.zeros(shape=(n_samp, (1 + 2 * delta_h) * n_dim))
    for idx in range(n_samp):
        for i_h in range(2 * delta_h + 1):
            idx_x = (idx + (i_h - delta_h)) % n_samp
            new_x[idx, i_h * n_dim:(i_h + 1) * n_dim] = x[idx_x]

    return new_x


def separate_weekend(x, y, x_test):
    """ Separate the datasets in two : week and weekend/holidays.
    The first two columns of the data must be the boolean for weekend and holidays.

    :param x:
    :param y:
    :param x_test:
    :return:
    """
    idx_week = []
    idx_weekend = []
    for i in range(x.shape[0]):
        x_s = x[i]
        bool_weekend = x_s[0]
        bool_holidays = x_s[1]
        if bool_holidays or bool_weekend:
            idx_weekend.append(i)
        else:
            idx_week.append(i)

    x_week = x[idx_week, 2:]
    x_weekend = x[idx_weekend, 2:]
    y_week = y[idx_week]
    y_weekend = y[idx_weekend]

    idx_t_week = []
    idx_t_weekend = []

    for i in range(x_test.shape[0]):
        x_s = x_test[i]
        bool_weekend = x_s[0]
        bool_holidays = x_s[1]
        if bool_holidays or bool_weekend:
            idx_t_weekend.append(i)
        else:
            idx_t_week.append(i)

    x_t_week = x_test[idx_t_week, 2:]
    x_t_weekend = x_test[idx_t_weekend, 2:]

    return x_week, x_weekend, y_week, y_weekend, x_t_week, x_t_weekend, idx_t_week, idx_t_weekend
