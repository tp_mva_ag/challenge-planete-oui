"""
Code created by Antonin GAY <antonin.gay@gmail.com>

04/2019
"""

__author__ = 'Antonin GAY'

from src.data import *
import src.models_cross_val as models_cv
from src.plot import *

# Boolean if the training must be different for week and weekend/holidays
sep_weekend = False

if __name__ == '__main__':
    # model_name_list =
    # ['lin_reg', 'mlp', 'dec_tree', 'ridge', 'mlp_1', 'mlp_1_sig', 'mlp_2', 'mlp_5', 'poly', 'xgboost']
    #
    # See model_cv.get_y_test for more details
    model_name = 'lin_reg'

    x_dict = load_x_csv('data/input_training_ssnsrY0.csv')
    y_dict = load_y_csv('data/output_training_Uf11I9I.csv')
    x_test_dict = load_x_csv('data/input_test_cdKcI0e.csv')

    # plot_1(x_dict, y_dict, x_test_dict)

    read_timestamp(x_dict)
    read_timestamp(x_test_dict)

    define_holidays(x_dict, [1272, 6478], [1487, 7030])
    define_holidays(x_test_dict, [1248, 6455], [1463, 7006])

    data_col_test_1 = ['temp_1', 'temp_2', 'mean_national_temp', 'humidity_1', 'humidity_2',
                       'consumption_secondary_1', 'consumption_secondary_2', 'consumption_secondary_3']
    data_col_test_2 = ['hour', 'day', 'month',
                       'temp_1', 'temp_2', 'mean_national_temp', 'humidity_1', 'humidity_2',
                       'consumption_secondary_1', 'consumption_secondary_2', 'consumption_secondary_3']
    data_col_test_3 = ['hour', 'day', 'month', 'weekend', 'holidays',
                       'temp_1', 'temp_2', 'mean_national_temp', 'humidity_1', 'humidity_2',
                       'consumption_secondary_1', 'consumption_secondary_2', 'consumption_secondary_3']

    data_col_test_4 = ['hour', 'day', 'month', 'weekend', 'week', 'daytime', 'night',
                       'temp_1', 'temp_2', 'mean_national_temp', 'humidity_1', 'humidity_2',
                       'consumption_secondary_1', 'consumption_secondary_2', 'consumption_secondary_3']

    data_col_test_5 = ['weekend', 'holidays',
                       'temp_1', 'temp_2', 'mean_national_temp', 'humidity_1', 'humidity_2',
                       'consumption_secondary_1', 'consumption_secondary_2', 'consumption_secondary_3']

    data_col_to_use = data_col_test_5

    x = dict_to_array(x_dict, data_col_to_use)
    y = dict_to_array(y_dict, ['consumption_1', 'consumption_2'])
    x_test = dict_to_array(x_test_dict, data_col_to_use)

    if sep_weekend:
        x_week, x_weekend, y_week, y_weekend, x_t_week, x_t_weekend, idx_t_week, idx_t_weekend = \
            separate_weekend(x, y, x_test)

        y_t_week = models_cv.get_y_test(model_name, x_week, y_week, x_t_week)
        y_t_weekend = models_cv.get_y_test(model_name, x_weekend, y_weekend, x_t_weekend)

        y_test = np.zeros(shape=(x_test.shape[0], 2))

        y_test[idx_t_week] = y_t_week
        y_test[idx_t_weekend] = y_t_weekend
    else:
        y_test = models_cv.get_y_test(model_name, x, y, x_test)

    y_array_to_csv(y_test, 'results/output_' + model_name + '.csv')

    plt.figure()
    plt.subplot(221)
    plt.plot(y_test[:, 0], label='y0')
    plt.title('Test prediction #1')
    plt.legend()
    plt.subplot(223)
    plt.plot(y_test[:, 1], label='y1')
    plt.title('Test prediction #2')
    plt.legend()

    plt.subplot(122)
    plt.plot(x_test_dict.consumption_secondary_1, label='x1')
    plt.plot(x_test_dict.consumption_secondary_2, label='x2')
    plt.plot(x_test_dict.consumption_secondary_3, label='x3')

    plt.title('Test consumptions data')
    plt.legend()

    plt.show()
