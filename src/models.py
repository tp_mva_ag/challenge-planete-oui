from sklearn.linear_model import LinearRegression
from sklearn.neural_network import MLPRegressor
from sklearn import tree

from sklearn.model_selection import train_test_split


def fit_predict(reg, x, y, x_test, test_size=0.2):
    """ Fit a model to the given data, separating them into train and validation sets. Then predict the test set

    :param reg:
    :param x:
    :param y:
    :param x_test:
    :param test_size:
    :return:
    """
    x_train, x_val, y_train, y_val = train_test_split(x, y, test_size=test_size, shuffle=True, random_state=42)

    reg.fit(x_train, y_train)

    print('Regression score on train set = %.3f' % reg.score(x_train, y_train))
    print('Regression score on validation set = %.3f' % reg.score(x_val, y_val))

    y_test = reg.predict(x_test)

    return y_test


def lin_reg(x, y, x_test, test_size=0.2):
    """ Train a Linear regression

    :param x:
    :param y:
    :param x_test:
    :return:
    """

    reg = LinearRegression()

    return fit_predict(reg, x, y, x_test, test_size)


# todo: test new hiden layers size for MLP
def mlp_reg(x, y, x_test, test_size=0.2,
            hidden_layer_sizes=(256, 256, 128)):
    """ Train a Multi-Layer Neural network for regression

    :param x:
    :param y:
    :param x_test:
    :return:
    """

    reg = MLPRegressor(hidden_layer_sizes=hidden_layer_sizes, verbose=True, max_iter=200, n_iter_no_change=50)

    return fit_predict(reg, x, y, x_test, test_size)


def dec_tree(x, y, x_test, test_size=0.2):
    """ Train a Decidion Tree regression

    :param x:
    :param y:
    :param x_test:
    :return:
    """

    reg = tree.DecisionTreeRegressor(min_samples_leaf=3)

    return fit_predict(reg, x, y, x_test, test_size)
