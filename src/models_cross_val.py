import sklearn.utils
import matplotlib.pyplot as plt
import numpy as np
from sklearn import tree, linear_model
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_val_predict
from sklearn.neural_network import MLPRegressor
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.preprocessing import PolynomialFeatures
from xgboost import XGBRegressor

from src.custom_metric import w_mae_array


def undo_shuffle(y, idx):
    new_y = np.zeros_like(y)
    for i, j in enumerate(idx):
        new_y[j] = y[i]

    return new_y


def cross_val_test(reg, x, y, x_test, plot=False):
    """ Do cross_validation on a regression model, and then fit and predict

    :param reg: The Scikit learn regression model
    :param x: The train data
    :param y: The train target
    :param x_test: The data to be predicted
    :return y_test: The predicted targets
    :param plot: boolean
    """

    idx = np.linspace(0, x.shape[0] - 1, num=x.shape[0], dtype=int)

    # Shuffled data
    x_sh, y_sh, idx_sh = sklearn.utils.shuffle(x, y, idx)
    # scores = cross_val_score(reg, x, y, cv=4, n_jobs=-1)
    # print("Accuracy: %0.3f (+/- %0.3f)" % (scores.mean(), scores.std() * 2))

    preds_sh = cross_val_predict(reg, x_sh, y_sh, cv=4, n_jobs=-1)

    if plot:
        # We un shuffle the data to plot them
        y_un_sh = undo_shuffle(y_sh, idx_sh)
        preds_un_sh = undo_shuffle(preds_sh, idx_sh)

        plt.figure()
        plt.subplot(211)
        plt.plot(y_un_sh - preds_un_sh)
        plt.title('Prediction error')

        plt.subplot(212)
        plt.hist(y_un_sh - preds_un_sh, 100)
        plt.title('Prediction error')

        plt.figure()
        plt.subplot(211)
        plt.plot(y_un_sh[idx, 0], label='Ground Truth')
        plt.plot(preds_un_sh[idx, 0], label='Prediction')
        plt.title('Cross validation comparison')
        plt.legend()

        plt.subplot(212)
        plt.plot(y_un_sh[idx, 1], label='Ground Truth')
        plt.plot(preds_un_sh[idx, 1], label='Prediction')
        plt.title('Cross validation comparison')
        plt.legend()

    print("Cross-Validation - Custom Metrics (W MAE): %0.3f" % w_mae_array(y_sh, preds_sh))

    reg.fit(x, y)
    y_test = reg.predict(x_test)

    return y_test


def lin_reg(x, y, x_test):
    """ Train a Linear regression, and evaluate it by cross validation

    :param x:
    :param y:
    :param x_test:
    :return:
    """
    reg = LinearRegression()

    return cross_val_test(reg, x, y, x_test)


def polyn_reg(x, y, x_test, degree=2):
    reg = make_pipeline(PolynomialFeatures(degree), linear_model.RidgeCV())

    return cross_val_test(reg, x, y, x_test)


def ridge(x, y, x_test):
    """ Train a Ridge CV regression, and evaluate it by cross validation

    :param x:
    :param y:
    :param x_test:
    :return:
    """
    reg = linear_model.RidgeCV(alphas=[0.1, 1.0, 10.0], cv=3)

    return cross_val_test(reg, x, y, x_test)


def mlp_reg(x, y, x_test, hidden_layer_sizes=(64, 32, 32), activation='relu', verbose=False, max_epochs=300):
    """ Train a Multi-Layer Neural network for regression, validating it with cross validation

    :param activation:
    :param x:
    :param y:
    :param x_test:
    :param hidden_layer_sizes:
    :param verbose:
    :param max_epochs:
    :return:
    """
    reg = MLPRegressor(hidden_layer_sizes=hidden_layer_sizes, activation=activation,
                       verbose=verbose, max_iter=max_epochs, n_iter_no_change=10)

    return cross_val_test(reg, x, y, x_test)


def dec_tree(x, y, x_test):
    """ Train a Decision Tree regression, and evaluate it by cross validation

    :param x:
    :param y:
    :param x_test:
    :return:
    """

    # reg = tree.DecisionTreeRegressor(criterion='mae', min_samples_split=50, min_weight_fraction_leaf=0.2)
    reg = tree.DecisionTreeRegressor(criterion='mse', max_depth=16, min_samples_leaf=20, min_samples_split=50)

    return cross_val_test(reg, x, y, x_test)


def ploy_reg(x, y, x_test):
    reg = Pipeline([('poly', PolynomialFeatures(degree=2)),
                    ...('linear', LinearRegression(fit_intercept=False))])

    return cross_val_test(reg, x, y, x_test)


def get_y_test(model_name, x, y, x_test):
    if model_name == 'lin_reg':
        y_test = lin_reg(x, y, x_test)
    elif model_name == 'mlp':
        y_test = mlp_reg(x, y, x_test, hidden_layer_sizes=(256, 128, 32), verbose=True)
    elif model_name == 'dec_tree':
        y_test = dec_tree(x, y, x_test)
    elif model_name == 'ridge':
        y_test = ridge(x, y, x_test)
    elif model_name == 'mlp_1':
        y_test = mlp_reg(x, y, x_test, 4, verbose=True)
    elif model_name == 'mlp_1_sig':
        y_test = mlp_reg(x, y, x_test, 64, activation='logistic', verbose=True, max_epochs=1000)
    elif model_name == 'mlp_2':
        y_test = mlp_reg(x, y, x_test, (4, 2), activation='relu', verbose=True, max_epochs=5000)
    elif model_name == 'mlp_5':
        y_test = mlp_reg(x, y, x_test, (64, 32, 16, 8, 4), activation='relu', verbose=True, max_epochs=5000)
    elif model_name == 'poly':
        y_test = polyn_reg(x, y, x_test, degree=2)
    elif model_name == 'xgboost':
        y_test = xgboost(x, y, x_test)
        # y_test_2 = xgboost(x, y[:, 1], x_test)
        #
        # y_test = np.zeros(shape=(x_test.shape[0], 2))
        # y_test[:, 0] = y_test_1
        # y_test[:, 1] = y_test_2
    else:
        y_test = lin_reg(x, y, x_test)

    return y_test


def multi_models(model_name_list, x, y, x_test):
    nb_models = len(model_name_list)

    y_test = np.zeros(shape=(nb_models, x_test.shape[0], 2))
    for idx, model_name in enumerate(model_name_list):
        y_test[idx] = get_y_test(model_name, x, y, x_test)

    y_test = y_test.mean(axis=0)

    return y_test


def xgboost(x, y, x_test):
    reg = XGBRegressor()

    return cross_val_test(reg, x, y, x_test)
