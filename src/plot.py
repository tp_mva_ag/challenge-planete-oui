import matplotlib.pyplot as plt


def plot_1(x_dict, y_dict, x_test):
    # Consumptions
    plt.figure()
    plt.subplot(221)
    plt.plot(x_dict.consumption_secondary_1, label='x1')
    plt.plot(x_dict.consumption_secondary_2, label='x2')
    plt.plot(x_dict.consumption_secondary_3, label='x3')
    plt.legend()
    plt.title('Consumption of the secondary sites')

    if y_dict is not None:
        plt.subplot(222)
        plt.plot(y_dict.consumption_1, label='y1')
        plt.plot(y_dict.consumption_2, label='y2')
        plt.legend()
        plt.title('Consumption of the main sites')

    plt.subplot(212)
    plt.plot(x_dict.consumption_secondary_1[:1000], label='x1')
    plt.plot(x_dict.consumption_secondary_2[:1000], label='x2')
    plt.plot(x_dict.consumption_secondary_3[:1000], label='x3')
    if y_dict is not None:
        plt.plot(y_dict.consumption_1[:1000], label='y1')
        plt.plot(y_dict.consumption_2[:1000], label='y2')
    plt.legend()
    plt.title('Consumption all the sites, zoomed')

    # Temperatures & Humidity Zoomed
    plt.figure()
    plt.subplot(211)
    plt.plot(x_dict.temp_1[4500:6000], label='Temp 1')
    plt.plot(x_dict.temp_2[4500:6000], label='Temp 2')
    plt.legend()
    plt.title('Temperatures on the main sites (zoom)')

    plt.subplot(212)
    plt.plot(x_dict.humidity_1[4500:6000], label='Humidity 1')
    plt.plot(x_dict.humidity_2[4500:6000], label='Humidity 2')
    plt.legend()
    plt.title('Humidities on the main sites (zoom)')

    # Temperatures & Humidity
    plt.figure()
    plt.subplot(221)
    plt.plot(x_dict.temp_1, label='Temp 1')
    plt.plot(x_dict.temp_2, label='Temp 2')
    plt.legend()
    plt.title('Temperatures on the main sites (full)')

    plt.subplot(222)
    plt.plot(x_test.temp_1, label='Temp Test 1')
    plt.plot(x_test.temp_2, label='Temp Test 2')
    plt.legend()
    plt.title('Temperatures on the main sites (full)')

    plt.subplot(223)
    plt.plot(x_dict.humidity_1, label='Humidity 1')
    plt.plot(x_dict.humidity_2, label='Humidity 2')
    plt.legend()
    plt.title('Humidities on the main sites (full)')
    plt.subplot(224)
    plt.plot(x_test.humidity_1, label='Humidity Test 1')
    plt.plot(x_test.humidity_2, label='Humidity Test 2')
    plt.legend()
    plt.title('Humidities on the main sites (full)')

    plt.figure()
    plt.suptitle('Comparison consumption secondary Train / Test')
    plt.subplot(221)
    plt.plot(x_dict.consumption_secondary_1, label='Train set')
    plt.plot(x_test.consumption_secondary_1, label='Test set')
    plt.title('Consumption Secondary #1')
    plt.legend()

    plt.subplot(222)
    plt.plot(x_dict.consumption_secondary_2, label='Train set')
    plt.plot(x_test.consumption_secondary_2, label='Test set')
    plt.title('Consumption Secondary #2')
    plt.legend()

    plt.subplot(212)
    plt.plot(x_dict.consumption_secondary_3, label='Train set')
    plt.plot(x_test.consumption_secondary_3, label='Test set')
    plt.title('Consumption Secondary #3')
    plt.legend()

    plt.show()
