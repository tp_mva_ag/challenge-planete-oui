"""
Code written by the authors of the sklearn library as an example of use of Random search
"""

from time import time
from scipy.stats import randint as sp_randint
from sklearn import tree
from sklearn.metrics import make_scorer
from sklearn.model_selection import RandomizedSearchCV
from sklearn.neural_network import MLPRegressor

from src.custom_metric import w_mae_array

# Utility function to report best scores
from src.data import *


def report(results, n_top=3):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                results['mean_test_score'][candidate],
                results['std_test_score'][candidate]))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")


def random_search(reg, param_distr, x, y):
    # run randomized search
    n_iter_search = 1
    score = make_scorer(w_mae_array, greater_is_better=False)

    random_s = RandomizedSearchCV(reg, param_distributions=param_distr,
                                  n_iter=n_iter_search, cv=2,
                                  scoring=score)

    # x, y = shuffle(x, y)
    start = time.time()
    random_s.fit(x, y)
    print("RandomizedSearchCV took %.2f seconds for %d candidates"
          " parameter settings." % ((time.time() - start), n_iter_search))
    report(random_s.cv_results_)


def decision_tree_rs(x, y):
    # specify parameters and distributions to sample from
    param_distr = {"max_depth": sp_randint(1, 20),
                   "max_features": sp_randint(1, x.shape[1]),
                   "min_samples_leaf": sp_randint(1, 20),
                   "min_samples_split": sp_randint(2, 50)}

    reg = tree.DecisionTreeRegressor(criterion='mae')

    random_search(reg, param_distr, x, y)


def mlp_rs(x, y):
    param_distributions = {
        'hidden_layer_sizes': [(50, 50, 50), (50, 100, 50), (100,)],
        'activation': ['tanh', 'relu'],
        'solver': ['sgd', 'adam'],
        'alpha': [0.0001, 0.05],
        'learning_rate': ['constant', 'adaptive'],
    }

    reg = MLPRegressor()

    random_search(reg, param_distributions, x, y)


if __name__ == '__main__':
    x_dict = load_x_csv('data/input_training_ssnsrY0.csv')
    y_dict = load_y_csv('data/output_training_Uf11I9I.csv')
    x_test_dict = load_x_csv('data/input_test_cdKcI0e.csv')

    # plot_1(x_dict, y_dict, x_test_dict)

    read_timestamp(x_dict)
    read_timestamp(x_test_dict)

    define_holidays(x_dict, [1272, 6478], [1487, 7030])
    define_holidays(x_test_dict, [1248, 6455], [1463, 7006])

    data_col_test_3 = ['hour', 'month', 'weekend',
                       'temp_1', 'temp_2', 'mean_national_temp', 'humidity_1', 'humidity_2',
                       'consumption_secondary_1', 'consumption_secondary_3']
    data_col_test_4 = ['hour', 'day', 'month', 'weekend', 'week', 'daytime', 'night',
                       'temp_1', 'temp_2', 'mean_national_temp', 'humidity_1', 'humidity_2',
                       'consumption_secondary_1', 'consumption_secondary_2', 'consumption_secondary_3']

    data_col_to_use = data_col_test_4

    x_m = dict_to_array(x_dict, data_col_to_use)
    y_m = dict_to_array(y_dict, ['consumption_1', 'consumption_2'])
    x_test_m = dict_to_array(x_test_dict, data_col_to_use)

    decision_tree_rs(x_m, y_m)
